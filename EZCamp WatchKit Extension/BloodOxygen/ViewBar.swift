//
//  ViewBar.swift
//  EZCamp WatchKit Extension
//
//  Created by Tito Guntur Safirda on 27/07/21.
//

import SwiftUI

struct ViewBar: View {
    
    @State var percentage: CGFloat = 0
    var body: some View {
        ZStack{
            Color.black
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            VStack{
                ScrollView{
                    ZStack{
                        Pulsation()
                        Track()
                        Outline(percentage: percentage)
                        Label(percentage: percentage)
                    }
                    Spacer()
                    HStack {
                        Button("Check", action: {
                            self.percentage = CGFloat(100)
                        }).cornerRadius(20).background(Color(red: 255/255, green: 118/255, blue: 0/255, opacity: 1.0).cornerRadius(20.0).foregroundColor(.white).font(.caption.bold()))
                    }
                    .padding(.top, 10.0)
                }
                
                
            }
        }
    }
}
struct Outline: View {
    var percentage: CGFloat = 0
    var colors: [Color] = [Color.outlineColor]
    var body: some View{
        ZStack{
            Circle()
                .fill(Color.rgb(r: 25, g: 25, b: 25))
                .frame(width: 164, height: 164)
                .overlay(
                Circle()
                    .trim(from: 0, to: percentage * 0.01)
                    .stroke(style: StrokeStyle(lineWidth: 10, lineCap: .round, lineJoin: .round))
                    .fill(AngularGradient(gradient: .init(colors: colors), center: .center, startAngle: .zero, endAngle: .init(degrees: 360)))
                ).animation(.spring(response: 2.0, dampingFraction: 1.0, blendDuration: 1.0))
        }
    }
}

struct Label: View {
    var percentage: CGFloat = 0
    var body: some View{
        ZStack{
            VStack{
                Image("Icon_BloodOxyWhite")
                    .resizable(resizingMode: .stretch)
                    .aspectRatio(contentMode: .fit)
                    .padding(.trailing, 3.0)
                    .frame(width: 42, height: 42, alignment: .center)
                .clipShape(Circle())
                Text("Blood O2 Check").font(.system(size: 15)).fontWeight(.bold).foregroundColor(.white)
                HStack{
                    Text("calculating...").font(.system(size: 11)).fontWeight(.regular).foregroundColor(.secondary)
                    Text(String(format: "%.0f",percentage)).font(.system(size: 11)).fontWeight(.regular).foregroundColor(.secondary)
                    Text("%").font(.system(size: 11)).fontWeight(.regular).foregroundColor(.secondary)
                }
            }
            
        }
    }
}

struct Track: View {
    var Colors: [Color] = [Color.black]
    var body: some View{
        ZStack{
            Circle()
                .fill(Color.black)
                .frame(width: 164, height: 164)
                .overlay(
                Circle()
                    .stroke(style: StrokeStyle(lineWidth: 20))
                    .fill(AngularGradient(gradient: .init(colors: Colors), center: .center))
                )
        }
    }

}

struct Pulsation: View {
    @State private var pulsate = false
    var color: [Color] = [Color.pulsatingColor]
    var body: some View{
        ZStack {
            Circle()
                .fill(Color.black)
                .frame(width: 159, height: 159)
                .scaleEffect(pulsate ? 1.3 : 1.1)
                .animation(Animation.easeInOut(duration: 10.0).repeatForever(autoreverses: true))
                .onAppear{
                    self.pulsate.toggle()
                }
        }
    }
}


struct ViewBar_Previews: PreviewProvider {
    static var previews: some View {
        ViewBar()
    }
}
