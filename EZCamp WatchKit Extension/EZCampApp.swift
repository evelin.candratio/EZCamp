//
//  EZCampApp.swift
//  EZCamp WatchKit Extension
//
//  Created by Evelin Candratio on 13/07/21.
//

import SwiftUI

@main
struct EZCampApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
               ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
