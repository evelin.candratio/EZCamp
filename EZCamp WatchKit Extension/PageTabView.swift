//
//  PageTabView.swift
//  EZCamp WatchKit Extension
//
//  Created by Tito Guntur Safirda on 27/07/21.
//

import SwiftUI

struct PageTabView: View {
    var body: some View {
            TabView{
                Dashboard()
                ListFriendView()
            }.tabViewStyle(PageTabViewStyle())
    }
}

struct PageTabView_Previews: PreviewProvider {
    static var previews: some View {
        PageTabView()
    }
}


