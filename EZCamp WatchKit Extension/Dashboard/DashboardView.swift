//
//  SwiftUIView.swift
//  EZCamp WatchKit Extension
//
//  Created by Tito Guntur Safirda on 23/07/21.
//

import SwiftUI

struct Dashboard: View {
    let timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
    @State var text = ""
    @State var count: Int = 5
    @State var finishedText: String? = nil
        
        let layout = [
            GridItem(.flexible(minimum: 100)),
        ]
        var columns = Array(repeating: GridItem(.flexible()), count: 1)
        var body: some View {
            
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                        VStack (alignment: .leading, spacing: 0, content: {
                            HStack {
                                VStack (alignment: .leading, spacing: 0, content: {
                                    Text("Body Status").font(.system(size: 13)).padding(.top, 3.0)
                                    Text("Safe and Sound").font(.system(size: 15) .bold()).padding(.top, 3.0).frame(width: 128, height: .infinity , alignment: .topLeading)
                                    Text("No need to worried everything okay").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 128, height:40 , alignment: .topLeading).font(.system(size: 11))
                                })
                                Image("Icon_ FaseNormal")
                                    .padding(0.0)
                                    .frame(width: 24.0, height: 24.0)
                            }
                        }).frame(width: 170, height:70 , alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    
                    
//                    Text(finishedText ?? "\(count)")
//                        .onReceive(timer, perform: { _ in
//                            if count <= 1 {
//                                finishedText = "Yeah"
//                            } else {
//                                count -= 1
//                            }
//                        })
                    
                    
                    
                    NavigationLink(
                        destination: ViewBar(),
                        label: {
                            Text("Check Blood Oxygen")
                        }).cornerRadius(20).background(Color(red: 255/255, green: 118/255, blue: 0/255, opacity: 1.0).cornerRadius(20)).font(.caption.bold())
//
//                    
                    Spacer()
                        .padding(.vertical, 3.0)
                    VStack(alignment: .leading, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                        Divider()
                        Text("Detail").font(.system(size: 17).bold()).padding(.top, 3.0).foregroundColor(.secondary)
                        
                    })
                    VStack (alignment: .leading, spacing: 0, content: {
                        Text("Heart Rate").font(.system(size: 13)).padding(.top, 3.0)
                        HStack (alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            Image("icon")
                                .resizable(resizingMode: .stretch)
                                .aspectRatio(contentMode: .fit)
                                .padding(.trailing, 3.0)
                                .frame(width: 30, height: 30, alignment: .center)
                            .clipShape(Circle())
                            Text("63").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(.orange)
                            Text("bpm").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.orange)
                        })
                        Text("Real time updated").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 150, height:15 , alignment: .topLeading).font(.system(size: 11))
                        
                       
                        
                    }).frame(width: 170, height:50 , alignment: .leading).padding(.leading, 7.0).padding(.vertical, 7).padding(.bottom, 7.0).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    Spacer()
                    VStack (alignment: .leading, spacing: 0, content: {
                        Text("Body Temperature").font(.system(size: 13)).padding(.top, 3.0)
                        HStack (alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            Image("icon2")
                                .resizable(resizingMode: .stretch)
                                .aspectRatio(contentMode: .fit)
                                .padding(.trailing, 3.0)
                                .frame(width: 30, height: 30, alignment: .center)
                            .clipShape(Circle())
                            Text("36.4").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(.orange)
                            Text("°C").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.orange)
                        })
                        Text("Real time updated").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 150, height:15 , alignment: .topLeading).font(.system(size: 11))
                        
                       
                        
                    }).frame(width: 170, height:50 , alignment: .leading).padding(.leading, 7.0).padding(.vertical, 7).padding(.bottom, 7.0).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    Spacer()
                    VStack (alignment: .leading, spacing: 0, content: {
                        Text("Blood Oxygen").font(.system(size: 13)).padding(.top, 3.0)
                        HStack (alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            Image("icon3")
                                .resizable(resizingMode: .stretch)
                                .aspectRatio(contentMode: .fit)
                                .padding(.trailing, 3.0)
                                .frame(width: 30, height: 30, alignment: .center)
                            .clipShape(Circle())
                            Text("90").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(.orange)
                            Text("%").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.orange)
                        })
                        Text("5 minutes ago update").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 150, height:15 , alignment: .topLeading).font(.system(size: 11))
                        
                       
                        
                    }).frame(width: 170, height:50 , alignment: .leading).padding(.leading, 7.0).padding(.vertical, 7).padding(.bottom, 7.0).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    
                    
                }.navigationBarTitle(Text("Self"))
            }
            
                    
        }
}



struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        Dashboard()
    }
}
