//
//  NotificationView.swift
//  EZCamp WatchKit Extension
//
//  Created by Evelin Candratio on 13/07/21.
//

import SwiftUI

//Notif After instal App
struct NotificationView: View {
    var body: some View {
        VStack(spacing: 5){
            VStack(alignment: .leading, spacing: 0
                   ,  content: {
                    Text("Welcome John !").font(.system(size: 17)).fontWeight(.medium)
                    Text("Hey John nice to connect with you").font(.system(size: 15))
                    Text("1m ago").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.grayTextColor))
                   }).frame(width: 184, alignment: .leading).padding(.leading, 7).padding(.bottom, 14).padding(.top, 14).padding(.trailing, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(9)
            NavigationLink(
                destination: Dashboard(),
                label: {
                    //Display Struct with status "Medium Phase"
                    Text("Open apps").foregroundColor(Color(ColorsLibrary.orangeColor))
                }).frame(width: .infinity, height: .infinity).cornerRadius(9)
        }
    }
}

//Notif if status Dangerous Phase
struct NotificationViewRed: View {
    var body: some View {
        VStack(spacing: 5){
            VStack(alignment: .leading, spacing: 0
                   ,  content: {
                    Text("Detect Hypothermia!").font(.system(size: 17)).fontWeight(.medium)
                    Text("Dangerous Phase").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.redColor))
                    Text("We found that Michael show hypothermia symptoms").font(.system(size: 15))
                    Text("1m ago").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.grayTextColor))
                   })
            NavigationLink(
                destination: Dashboard(),
                label: {
                    //Display Struct with status "Medium Phase"
                    Text("Detail").foregroundColor(Color(ColorsLibrary.orangeColor))
                })
        }
    }
}

//Notif if status Medium Phase
struct NotificationViewYellow: View {
    var body: some View {
        VStack(spacing: 5){
            VStack(alignment: .leading, spacing: 0
                   ,  content: {
                    Text("Detect Hypothermia!").font(.system(size: 17)).fontWeight(.medium)
                    Text("Medium Phase").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.yellowColor))
                    Text("We found that Michael show hypothermia symptoms").font(.system(size: 15))
                    Text("1m ago").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.grayTextColor))
                   }).frame(width: 184, alignment: .leading).padding(.leading, 7).padding(.bottom, 14).padding(.top, 14).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(9)
            Button("Detail"){
            }.cornerRadius(9).foregroundColor(Color(ColorsLibrary.orangeColor))
        }
    }
}

//Notif if status Light Phase
struct NotificationViewBlue: View {
    var body: some View {
        VStack(spacing: 5){
            VStack(alignment: .leading, spacing: 0
                   ,  content: {
                    Text("Detect Hypothermia!").font(.system(size: 17)).fontWeight(.medium)
                    Text("Light Phase").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.blueColor))
                    Text("We found that Michael show hypothermia symptoms").font(.system(size: 15))
                    Text("1m ago").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.grayTextColor))
                   }).frame(width: 184, alignment: .leading).padding(.leading, 7).padding(.bottom, 14).padding(.top, 14).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(9)
            Button("Detail"){
            }.cornerRadius(9).foregroundColor(Color(ColorsLibrary.orangeColor))
        }
    }
}

//Notif if status Check Blood Oxygen
struct NotificationViewBlood: View {
    var body: some View {
        VStack(spacing: 5){
            VStack(alignment: .leading, spacing: 0
                   ,  content: {
                    Text("Attantion!").font(.system(size: 17)).fontWeight(.medium)
                    Text("Need to check Blood Oxygen").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.blueColor))
                    Text("Hi! Handy need to check he’s blood oxygen").font(.system(size: 15))
                    Text("1m ago").font(.system(size: 15)).foregroundColor(Color(ColorsLibrary.grayTextColor))
                   }).frame(width: 184, alignment: .leading).padding(.leading, 7).padding(.bottom, 14).padding(.top, 14).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(9)
            Button("Check"){
            }.cornerRadius(9).foregroundColor(Color(ColorsLibrary.orangeColor))
        }
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationViewRed()
    }
}
