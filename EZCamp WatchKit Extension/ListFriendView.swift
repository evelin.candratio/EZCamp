//
//  ListFriendView.swift
//  EZCamp
//
//  Created by Steven Tan on 21/07/21.
//

import SwiftUI
import UserNotifications

struct ListFriendView: View {
    //Varible text using to check data
    @State var text = ""
    //Varible columns using to save collect data in Array
    var columns = Array(repeating: GridItem(.flexible()), count: 1)
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            //Status and list friend
            VStack {
                //Button Sort by
                VStack(alignment: .leading, spacing: 0
                       ,  content: {
                        Text("Sort by").font(.system(size: 13))
                        Text("Name").font(.system(size: 10))
                       }).frame(width: 170, height: 43, alignment: .leading).padding(.leading, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(5)
                //END Button Sort by
                
                LazyVGrid(columns: columns){
                    ForEach(BodyConditionData.filter({"\($0)".lowercased().contains(text.lowercased()) || text.isEmpty})){ i in
                        ListFriendBar(mystatusCondition: i)
                    }
                }
                
                //Button Triger notif try
                TempatTRYView()
                //END try
                
            }.padding(.top, 4) .navigationBarTitle(Text("Team"))
            //END Status and list friend
        }
        //END Scroll View
    }
}

struct ListFriendView_Previews: PreviewProvider {
    static var previews: some View {
        ListFriendView()
    }
}

//datadummy set
// ObservableObject
class StatusCondition: Identifiable, ObservableObject{
    var id = UUID()
    var UserName: String
    var HeartRate: Int
    var BodyTemp: Int
    var BloodOxy: Int
    var TimeBloodOxy: String
    var Status: String
    
    init(UserName:String, HeartRate:Int,BodyTemp:Int,BloodOxy:Int,TimeBloodOxy:String, Status:String) {
        self.UserName = UserName
        self.HeartRate = HeartRate
        self.BodyTemp = BodyTemp
        self.BloodOxy = BloodOxy
        self.TimeBloodOxy = TimeBloodOxy
        self.Status = Status
    }
    
}

var BodyConditionData = [
    StatusCondition(UserName: "Evelin Candratio", HeartRate: 92, BodyTemp: 36, BloodOxy: 98, TimeBloodOxy: "15:30", Status: "Dangerous Phase"),
    StatusCondition(UserName: "Marshel Aditya", HeartRate: 92, BodyTemp: 36, BloodOxy: 98, TimeBloodOxy: "15:30", Status: "Medium Phase"),
    StatusCondition(UserName: "Septia Rosa", HeartRate: 92, BodyTemp: 36, BloodOxy: 98, TimeBloodOxy: "15:30", Status: "Light Phase"),
    StatusCondition(UserName: "Steven Tan", HeartRate: 92, BodyTemp: 36, BloodOxy: 98, TimeBloodOxy: "15:30", Status: "Need to Check Blood Oxygen"),
    StatusCondition(UserName: "Tito Guntur", HeartRate: 92, BodyTemp: 36, BloodOxy: 98, TimeBloodOxy: "15:30", Status: "Safe and Sound"),
    StatusCondition(UserName: "Adam Gilbert", HeartRate: 92, BodyTemp: 36, BloodOxy: 98, TimeBloodOxy: "15:30", Status: "Safe and Sound"),
]
