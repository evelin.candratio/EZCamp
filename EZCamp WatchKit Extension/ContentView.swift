//
//  ContentView.swift
//  EZCamp WatchKit Extension
//
//  Created by Evelin Candratio on 13/07/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        PageTabView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
