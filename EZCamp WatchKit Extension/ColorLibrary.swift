//
//  ColorLibrary.swift
//  EZCamp
//
//  Created by Steven Tan on 23/07/21.
//

import Foundation
import UIKit

struct ColorsLibrary {
    static let orangeColor = UIColor( red: 255/255, green: 118/255, blue: 0/255, alpha: 1.0)
    static let blueColor = UIColor( red: 54/255, green: 152/255, blue: 244/255, alpha: 1.0)
    static let yellowColor = UIColor( red: 255/255, green: 169/255, blue: 0/255, alpha: 1.0)
    static let redColor = UIColor( red: 255/255, green: 69/255, blue: 58/255, alpha: 1.0)
    static let grayItemColor = UIColor( red: 28/255, green: 28/255, blue: 30/255, alpha: 1.0)
    static let grayTextColor = UIColor( red: 142/255, green: 142/255, blue: 147/255, alpha: 1.0)
}


