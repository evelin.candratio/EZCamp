//
//  AmbilValueConnectData.swift
//  EZCamp WatchKit Extension
//
//  Created by Steven Tan on 26/07/21.
//

import SwiftUI


struct AmbilValueConnectData: View {
    @Binding var Username : StatusCondition
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    //Body Condition Status
                    
                    VStack {
                        HStack {
                            VStack (alignment: .leading, spacing: 0, content: {
                                //Subtitle "Body Status"
                                Text("Body Status").font(.system(size: 13)).padding(.top, 3.0)
                                //HStack to check status
                                if Username.Status == "Dangerous Phase" {
                                    Text(Username.Status).font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor)).frame(width: 128, height: .infinity , alignment: .topLeading)
                                } else if Username.Status == "Medium Phase" {
                                    Text(Username.Status).font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor)).frame(width: 128, height: .infinity , alignment: .topLeading)
                                } else if Username.Status == "Light Phase" {
                                    Text(Username.Status).font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor)).frame(width: 128, height: .infinity , alignment: .topLeading)
                                } else if Username.Status == "Need to Check Blood Oxygen" {
                                    Text(Username.Status).font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor)).frame(width: 128, height: .infinity , alignment: .topLeading)
                                } else if Username.Status == "Safe and Sound" {
                                    Text(Username.Status).font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.white).frame(width: 128, height: .infinity , alignment: .topLeading)
                                }
                                //Notes Check Status Description
                                if Username.Status == "Dangerous Phase" {
                                    Text("Your friend is in critical condition!").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 128, height:40 , alignment: .topLeading).font(.system(size: 11)).frame(width: 128, height: .infinity , alignment: .topLeading).font(.system(size: 11))
                                } else if Username.Status == "Medium Phase" {
                                    Text("Your friend has bad condition").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 128, height:40 , alignment: .topLeading).font(.system(size: 11)).frame(width: 128, height: .infinity , alignment: .topLeading).font(.system(size: 11))
                                } else if Username.Status == "Light Phase" {
                                    Text("Your friend has unnormal condition").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 128, height:40 , alignment: .topLeading).font(.system(size: 11)).frame(width: 128, height: .infinity , alignment: .topLeading).font(.system(size: 11))
                                } else if Username.Status == "Need to Check Blood Oxygen" {
                                    Text("Ensure to check it correctly!").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 128, height:40 , alignment: .topLeading).font(.system(size: 11)).frame(width: 128, height: .infinity , alignment: .topLeading).font(.system(size: 11))
                                } else if Username.Status == "Safe and Sound" {
                                    Text("Woohoo!! Enjoy your hiking").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 128, height:40 , alignment: .topLeading).font(.system(size: 11)).frame(width: 128, height: .infinity , alignment: .topLeading).font(.system(size: 11))
                                }
                            })
                            
                            if Username.Status == "Dangerous Phase" {
                                Image("Icon_DangerRed")
                                    .padding(0.0)
                                    .frame(width: 24.0, height: 24.0)
                            } else if Username.Status == "Medium Phase" {
                                Image("Icon_DangerYellow")
                                    .padding(0.0)
                                    .frame(width: 24.0, height: 24.0)
                            } else if Username.Status == "Light Phase" {
                                Image("Icon_DangerBlue")
                                    .padding(0.0)
                                    .frame(width: 24.0, height: 24.0)
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Image("Icon_FaseCheck")
                                    .padding(0.0)
                                    .frame(width: 24.0, height: 24.0)
                            } else if Username.Status == "Safe and Sound" {
                                Image("Icon_ FaseNormal")
                                    .padding(0.0)
                                    .frame(width: 24.0, height: 24.0)
                            }
                        }
                    }.frame(width: 170, height:70 , alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    //END VStack Body Condition Status
                    
                    
                    //                    Button("Check Blood Oxygen"){
                    //                    }.cornerRadius(20).background(Color(red: 255/255, green: 118/255, blue: 0/255, opacity: 1.0).cornerRadius(20)).font(.caption.bold())
                    
                    Spacer()
                        .padding(.vertical, 3.0)
                    
                    //Divider and SubTitle "Detail"
                    VStack(alignment: .leading, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                        Divider()
                        Text("Detail").font(.system(size: 17).bold()).padding(.top, 3.0).foregroundColor(.secondary)
                        
                    })
                    
                    //VStack Detail Body Condition
                    //HeartRate Detail
                    VStack (alignment: .leading, spacing: 0, content: {
                        Text("Heart Rate").font(.system(size: 13)).padding(.top, 3.0)
                        HStack (alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            //HeartRateIconCheck
                            if Username.Status == "Dangerous Phase" {
                                Image("Icon_HeartRateRed")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Medium Phase" {
                                Image("Icon_HeartRateYellow")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            }else if Username.Status == "Light Phase" {
                                Image("Icon_HeartRateBlue")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Image("Icon_HeartRateBlue")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Safe and Sound" {
                                Image("Icon_HeartRateWhite")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            }
                            
                            //HeartRateValueCheck
                            if Username.Status == "Dangerous Phase" {
                                Text("\(Username.HeartRate)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor))
                            } else if Username.Status == "Medium Phase" {
                                Text("\(Username.HeartRate)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor))
                            } else if Username.Status == "Light Phase" {
                                Text("\(Username.HeartRate)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Text("\(Username.HeartRate)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Safe and Sound" {
                                Text("\(Username.HeartRate)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(.white)
                            }
                            
                            //HeartRateUnitCheck
                            if Username.Status == "Dangerous Phase" {
                                Text("bpm").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor))
                            } else if Username.Status == "Medium Phase" {
                                Text("bpm").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor))
                            } else if Username.Status == "Light Phase" {
                                Text("bpm").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Text("bpm").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Safe and Sound" {
                                Text("bpm").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.white)
                            }
                            
                        })
                        Text("Real time updated").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 150, height:15 , alignment: .topLeading).font(.system(size: 11))
                    }).frame(width: 170, height:50 , alignment: .leading).padding(.leading, 7.0).padding(.vertical, 7).padding(.bottom, 7.0).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    
                    
                    Spacer()
                    
                    //Body Temperature Detail
                    VStack (alignment: .leading, spacing: 0, content: {
                        Text("Body Temperature").font(.system(size: 13)).padding(.top, 3.0)
                        HStack (alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            //BodyTemperatureIconCheck
                            if Username.Status == "Dangerous Phase" {
                                Image("Icon_TempRed")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Medium Phase" {
                                Image("Icon_TempYellow")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            }else if Username.Status == "Light Phase" {
                                Image("Icon_TempBlue")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Image("Icon_TempBlue")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Safe and Sound" {
                                Image("Icon_TempWhite")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            }
                            
                            //BodyTemperatureValueCheck
                            if Username.Status == "Dangerous Phase" {
                                Text("\(Username.BodyTemp)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor))
                            } else if Username.Status == "Medium Phase" {
                                Text("\(Username.BodyTemp)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor))
                            } else if Username.Status == "Light Phase" {
                                Text("\(Username.BodyTemp)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Text("\(Username.BodyTemp)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Safe and Sound" {
                                Text("\(Username.BodyTemp)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(.white)
                            }
                            
                            //BodyTemperatureUnitCheck
                            if Username.Status == "Dangerous Phase" {
                                Text("°C").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor))
                            } else if Username.Status == "Medium Phase" {
                                Text("°C").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor))
                            } else if Username.Status == "Light Phase" {
                                Text("°C").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Text("°C").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Safe and Sound" {
                                Text("°C").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.white)
                            }
                            
                        })
                        Text("Real time updated").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 150, height:15 , alignment: .topLeading).font(.system(size: 11))
                    }).frame(width: 170, height:50 , alignment: .leading).padding(.leading, 7.0).padding(.vertical, 7).padding(.bottom, 7.0).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    
                    Spacer()
                    
                    //Blood Oxygen Detail
                    VStack (alignment: .leading, spacing: 0, content: {
                        Text("Blood Oxygen").font(.system(size: 13)).padding(.top, 3.0)
                        HStack (alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            //BloodOxygenIconCheck
                            if Username.Status == "Dangerous Phase" {
                                Image("Icon_BloodOxyRed")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Medium Phase" {
                                Image("Icon_BloodOxyYellow")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            }else if Username.Status == "Light Phase" {
                                Image("Icon_BloodOxyBlue")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Image("Icon_BloodOxyBlue")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            } else if Username.Status == "Safe and Sound" {
                                Image("Icon_BloodOxyWhite")
                                    .resizable(resizingMode: .stretch)
                                    .aspectRatio(contentMode: .fit)
                                    .padding(.trailing, 3.0)
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .clipShape(Circle())
                            }
                            
                            //BloodOxygenValueCheck
                            if Username.Status == "Dangerous Phase" {
                                Text("\(Username.BloodOxy)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor))
                            } else if Username.Status == "Medium Phase" {
                                Text("\(Username.BloodOxy)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor))
                            } else if Username.Status == "Light Phase" {
                                Text("\(Username.BloodOxy)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Text("\(Username.BloodOxy)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Safe and Sound" {
                                Text("\(Username.BloodOxy)").font(.system(size: 15).bold()).padding(.top, 3.0).foregroundColor(.white)
                            }
                            
                            //BloodOxygenUnitCheck
                            if Username.Status == "Dangerous Phase" {
                                Text("%").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.redColor))
                            } else if Username.Status == "Medium Phase" {
                                Text("%").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.yellowColor))
                            } else if Username.Status == "Light Phase" {
                                Text("%").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Need to Check Blood Oxygen" {
                                Text("%").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(Color(ColorsLibrary.blueColor))
                            } else if Username.Status == "Safe and Sound" {
                                Text("%").font(.system(size: 15) .bold()).padding(.top, 3.0).foregroundColor(.white)
                            }
                        })
                        HStack{
                            Text("\(Username.TimeBloodOxy)").foregroundColor(.secondary).padding(.top, 3.0).frame(width: 30, height:15 , alignment: .topLeading).font(.system(size: 11))
                            Text("minutes ago update").foregroundColor(.secondary).multilineTextAlignment(.leading).padding(.top, 3.0).frame(width: 150, height:15 , alignment: .topLeading).font(.system(size: 11))
                        }
                    }).frame(width: 170, height:50 , alignment: .leading).padding(.leading, 7.0).padding(.vertical, 7).padding(.bottom, 7.0).background(Color(red: 28.0/255, green: 28.0/255, blue: 28.0/255, opacity: 1.0)).cornerRadius(8)
                    
                    
                }
            }
        }.navigationBarTitle(Text(Username.UserName))
    }
}

//struct AmbilValueConnectData_Previews: PreviewProvider {
//    static var previews: some View {
//        AmbilValueConnectData(Username: Binding<StatusCondition>)
//    }
//}

