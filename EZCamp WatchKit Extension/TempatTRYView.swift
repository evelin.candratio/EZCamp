//
//  SwiftUIView.swift
//  EZCamp
//
//  Created by Steven Tan on 26/07/21.
//

import SwiftUI
import UserNotifications

// Coba Push Notif
struct TempatTRYView: View {
    var body: some View {
        Button("Dangerous Notif"){
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]){
                success, error in
                if success{
                    print("All set!")
                    
                    let content = UNMutableNotificationContent()
                    content.title = "This is your first notification"
                    content.body = "Test message"
                    content.sound = UNNotificationSound.default
                    content.categoryIdentifier = "myCategory"
                    content.threadIdentifier = "5280"
                    
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
                    
                    let request = UNNotificationRequest(identifier: "UUID().uuidString", content: content, trigger: trigger)
                    
                    UNUserNotificationCenter.current().add(request)
                } else if let error = error{
                    print(error.localizedDescription)
                }
                
            }
        }
    }
}

struct TempatTRYView_Previews: PreviewProvider {
    static var previews: some View {
        TempatTRYView()
    }
}

