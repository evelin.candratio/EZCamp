//
//  ListFriendBar.swift
//  EZCamp
//
//  Created by Steven Tan on 21/07/21.
//

import SwiftUI

//This Struct using to check value from dataset "Status(safe,dangerous,etc)", which is for determine the right conditions
struct ListFriendBar: View {
    
    //the variabel mystatusCondition using to collect the value from "BodyCondition" that we choose from ListFriendView page
    @State var mystatusCondition: StatusCondition = StatusCondition(UserName: "", HeartRate: 0, BodyTemp: 0, BloodOxy: 0, TimeBloodOxy: "", Status: "")
    
    //Code to represent the right condition, and link to personal detail
    var body: some View {
        //is the status is "Dangerous Phase" ?
        if mystatusCondition.Status == "Dangerous Phase"{
            NavigationLink(
                //destinate to Page "Persona Detail(AmbilValueConnectData)"
                //Username represent to collect the same value from "mystatusCondition"
                destination: AmbilValueConnectData(Username: $mystatusCondition),
                label: {
                    //Display Struct with status "Dangerous Phase"
                    DangerousCondition(mystatusCondition: mystatusCondition)
                }).frame(width: 170, height: 80)
            //is the status is "Medium Phase" ?
        } else if mystatusCondition.Status == "Medium Phase" {
            NavigationLink(
                destination: AmbilValueConnectData(Username: $mystatusCondition),
                label: {
                    //Display Struct with status "Medium Phase"
                    MediumCondition(mystatusCondition: mystatusCondition)
                }).frame(width: 170, height: 80)
            //is the status is "Light Phase" ?
        } else if mystatusCondition.Status == "Light Phase" {
            NavigationLink(
                destination: AmbilValueConnectData(Username: $mystatusCondition),
                label: {
                    //Display Struct with status "Light Phase"
                    LightCondition(mystatusCondition: mystatusCondition)
                }).frame(width: 170, height: 80)
            //is the status is "Need to Check Blood Oxygen" ?
        } else if mystatusCondition.Status == "Need to Check Blood Oxygen" {
            NavigationLink(
                destination: AmbilValueConnectData(Username: $mystatusCondition),
                label: {
                    //Display Struct with status "Need to Check Blood Oxygen"
                    BloodOxyCondition(mystatusCondition: mystatusCondition)
                }).frame(width: 170, height: 100)
            //is the status is "Safe and Sound" ?
        } else if mystatusCondition.Status == "Safe and Sound" {
            NavigationLink(
                destination: AmbilValueConnectData(Username: $mystatusCondition),
                label: {
                    //Display Struct with status "Safe and Sound"
                    NormalCondition(mystatusCondition: mystatusCondition)
                }).frame(width: 170, height: 80)
        }
    }
}

//struct using to display status "Dangerous Phase"
struct DangerousCondition: View {
    @State var mystatusCondition: StatusCondition = StatusCondition(UserName: "", HeartRate: 0, BodyTemp: 0, BloodOxy: 0, TimeBloodOxy: "", Status: "")
    var body: some View {
        //HStack for text(left side) and symbol(right side)
        HStack(alignment: .center, spacing: 4,  content: {
            //VStack Text(Username and Status with Description)
            VStack(alignment: .leading, spacing: 4
                   ,  content: {
                    Text(mystatusCondition.UserName).font(.system(size: 15)).bold()
                    //VStack Status with Description
                    VStack(alignment: .leading, spacing: 0
                           ,  content: {
                            Text(mystatusCondition.Status).font(.system(size: 13)).bold().foregroundColor(Color(ColorsLibrary.redColor))
                            Text("You need to prepared for the worse case").font(.system(size: 10)).foregroundColor(Color(ColorsLibrary.grayTextColor)).frame(width: 128, height: 24, alignment: .leading)
                           }).frame(maxWidth: 128, maxHeight: .infinity ,alignment: .leading)
                    //END VStack Status with Description
                   }).frame(minWidth: 128, maxHeight: .infinity ,alignment: .leading)
            //END VStack Username and Status with Description
            Image("Icon_DangerRed")
        }).frame(width: 170, height: .infinity, alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(5)
        //END HStack text(left side) and symbol(right side)
    }
}

//struct using to display status "Medium Phase"
struct MediumCondition: View {
    @State var mystatusCondition: StatusCondition = StatusCondition(UserName: "", HeartRate: 0, BodyTemp: 0, BloodOxy: 0, TimeBloodOxy: "", Status: "")
    var body: some View {
        HStack(alignment: .center, spacing: 4,  content: {
            VStack(alignment: .leading, spacing: 4
                   ,  content: {
                    Text(mystatusCondition.UserName).font(.system(size: 15)).bold()
                    VStack(alignment: .leading, spacing: 0
                           ,  content: {
                            Text(mystatusCondition.Status).font(.system(size: 13)).bold().foregroundColor(Color(ColorsLibrary.yellowColor))
                            Text("Your need to treat!!").font(.system(size: 10)).foregroundColor(Color(ColorsLibrary.grayTextColor)).frame(width: 128, height: 24, alignment: .leading)
                           }).frame(width: 128, alignment: .center)
                   }).frame(width: 128, alignment: .center)
            Image("Icon_DangerYellow")
        }).frame(width: 170, height: .infinity, alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(5)
    }
}

//struct using to display status "Light Phase"
struct LightCondition: View {
    @State var mystatusCondition: StatusCondition = StatusCondition(UserName: "", HeartRate: 0, BodyTemp: 0, BloodOxy: 0, TimeBloodOxy: "", Status: "")
    var body: some View {
        HStack(alignment: .center, spacing: 4,  content: {
            if mystatusCondition.Status == "Light Phase"{
                VStack(alignment: .leading, spacing: 4
                       ,  content: {
                        Text(mystatusCondition.UserName).font(.system(size: 15)).bold()
                        VStack(alignment: .leading, spacing: 0
                               ,  content: {
                                Text(mystatusCondition.Status).font(.system(size: 13)).bold().foregroundColor(Color(ColorsLibrary.blueColor))
                                Text("Your need to treat!!").font(.system(size: 10)).foregroundColor(Color(ColorsLibrary.grayTextColor)).frame(width: 128, height: 24, alignment: .leading)
                               }).frame(width: 128, alignment: .center)
                       }).frame(width: 128, alignment: .center)
                Image("Icon_DangerBlue")
            }
        }).frame(width: 170, height: .infinity, alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(5)
    }
}

//struct using to display status "Need to Check Blood Oxygen"
struct BloodOxyCondition: View {
    @State var mystatusCondition: StatusCondition = StatusCondition(UserName: "", HeartRate: 0, BodyTemp: 0, BloodOxy: 0, TimeBloodOxy: "", Status: "")
    var body: some View {
        HStack(alignment: .center, spacing: 4,  content: {
            VStack(alignment: .leading, spacing: 4
                   ,  content: {
                    Text(mystatusCondition.UserName).font(.system(size: 15)).bold()
                    VStack(alignment: .leading, spacing: 0
                           ,  content: {
                            Text(mystatusCondition.Status).font(.system(size: 13)).bold().foregroundColor(Color(ColorsLibrary.blueColor)).frame(minWidth: 128, idealWidth: 128, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: 34, idealHeight: 34, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading)
                            Text("You need to prepared for the worse case").font(.system(size: 10)).foregroundColor(Color(ColorsLibrary.grayTextColor)).frame(width: 128, height: 24, alignment: .leading)
                           }).frame(width: 128, alignment: .center)
                   }).frame(width: 128, alignment: .center)
            Image("Icon_FaseCheck")
        }).frame(width: 170, height: .infinity, alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(5)
    }
}

//struct using to display status "Safe and Sound"
struct NormalCondition: View {
    @State var mystatusCondition: StatusCondition = StatusCondition(UserName: "", HeartRate: 0, BodyTemp: 0, BloodOxy: 0, TimeBloodOxy: "", Status: "")
    var body: some View {
        HStack(alignment: .center, spacing: 4,  content: {
            VStack(alignment: .leading, spacing: 4
                   ,  content: {
                    Text(mystatusCondition.UserName).font(.system(size: 15)).bold()
                    VStack(alignment: .leading, spacing: 0
                           ,  content: {
                            Text(mystatusCondition.Status).font(.system(size: 13)).bold().foregroundColor(Color.white)
                            Text("No need to worried everything okay").font(.system(size: 10)).foregroundColor(Color(ColorsLibrary.grayTextColor)).frame(width: 128, height: 24, alignment: .leading)
                           }).frame(width: 128, alignment: .center)
                   }).frame(width: 128, alignment: .center)
            Image("Icon_ FaseNormal")
        }).frame(width: 170, height: .infinity, alignment: .leading).padding(.leading, 7).padding(.vertical, 7).padding(.bottom, 7).background(Color(ColorsLibrary.grayItemColor)).cornerRadius(5)
    }
}

//Just to help display the preview --->
struct ListFriendBar_Previews: PreviewProvider {
    static var previews: some View {
        ListFriendBar(mystatusCondition: BodyConditionData[3])
    }
}
