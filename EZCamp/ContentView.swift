//
//  ContentView.swift
//  EZCamp
//
//  Created by Evelin Candratio on 13/07/21.
//

import SwiftUI
import HealthKit

struct ContentView: View {
    private var healthStore : HealthStore?
    
    init() {
        healthStore = HealthStore()
        
    }
    var body: some View {
        NavigationView{
            VStack{

        WalkthroughView()
            }
            
        }
        .onAppear {
            if let healthStore = healthStore {
                healthStore.requestAuthorization {
                    success in
                }
            }
        }
        
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
