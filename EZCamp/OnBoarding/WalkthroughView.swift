//
//  WalkthroughView.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 15/07/21.
//

import SwiftUI

struct WalkthroughView: View {
    @State private var selection = 0
   // @Binding var isWalkthroughViewShowing: Bool
    
    var body: some View {
        ZStack{
            VStack{
                PageTabView(selection: $selection)
                ButtonView(selection: $selection)
            }
            .transition(.move(edge: .bottom))
            .padding()
           
        }
        .background(Color("BackgroundColor"))
        .ignoresSafeArea()
        
        
    }
    
}

//struct WalkthroughView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            WalkthroughView(isWalkthroughViewShowing: Binding.constant(true))
//            WalkthroughView(isWalkthroughViewShowing: Binding.constant(true))
//        }
//    }
//}
