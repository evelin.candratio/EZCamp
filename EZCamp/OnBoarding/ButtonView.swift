//
//  ButtonView.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 15/07/21.
//

import SwiftUI

struct ButtonView: View {
    @Binding var selection : Int
   // let buttons = ["Previous","Next"]
    var body: some View {
        HStack{
          //  ForEach(buttons, id: \.self ) { buttonLabel in
            if selection < tabs.count - 1 {
                Button (action: {selection += 1}, label: {
                    Text("Next")
                        .font(.system(.body, design: .rounded))
                        .bold()
                        .padding()
                        .frame(width: 343, height: 50)
                        .background(Color(ColorsLibrary.orangeColor))
                        .cornerRadius(12)
                        .padding(.horizontal)
                })
                
                
            }else {
                NavigationLink(
                    destination: MonitorView().preferredColorScheme(.dark),
                    label: {
                        Text("Let's Start")
                        .font(.system(.body, design: .rounded))
                        .bold()
                        .padding()
                        .frame(width: 343, height: 50)
                        .background(Color(ColorsLibrary.orangeColor))
                        .cornerRadius(12)
                        .padding(.horizontal)
                    });
                
                
            }
            
            
//                Button(action: {selection += 1}, label: {
//                    if selection < tabs.count - 1 {
//                        Text("Next")
//                            .font(.system(.body, design: .rounded))
//                            .bold()
//                            .padding()
//                            .frame(width: 343, height: 50)
//                            .background(Color(ColorsLibrary.orangeColor))
//                            .cornerRadius(12)
//                            .padding(.horizontal)
//
//                    }else {  Text("Create Account")
//                        .font(.system(.body, design: .rounded))
//                        .bold()
//                        .padding()
//                        .frame(width: 343, height: 50)
//                        .background(Color(ColorsLibrary.orangeColor))
//                        .cornerRadius(12)
//                        .padding(.horizontal)
//
//                    }
//
//                })
            
            
            
           // }
        }
        .foregroundColor(.white)
        .padding()
    }
//    func buttonAction(_ buttonLabel:String) {
//        withAnimation {
//            if buttonLabel == "Previous" && selection > 0 {
//                selection -= 1
//            } else if buttonLabel == "Next" && selection < tabs.count - 1 {
//                selection += 1
//            }
//
//        }
//    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(selection: Binding.constant(0))
    }
}
