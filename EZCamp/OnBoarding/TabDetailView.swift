//
//  TabDetailView.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 15/07/21.
//

import SwiftUI

struct TabDetailView: View {
    let index: Int
    
    var body: some View {
        ZStack{
            VStack {
                Image(tabs[index].image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 300)
                    .padding(.top,-100)
                   
                
                Text(tabs[index].title)
                    .font(.system(.title2, design: .rounded))
                    .bold()
                    .padding(.top)
                
                Text(tabs[index].text)
                    .font(.system(.caption, design: .rounded))
                    .foregroundColor(Color(ColorsLibrary.grayTextColor))
                    .fontWeight(.regular)
                    .multilineTextAlignment(.center)
                    .padding()

            }
          .foregroundColor(.white)
        }
//        .background(Color(red: 0.114, green: 0.114, blue: 0.114))
//        .ignoresSafeArea()
    }
        
}

struct TabDetailView_Previews: PreviewProvider {
    static var previews: some View {
            TabDetailView(index: 0)
    }
}
