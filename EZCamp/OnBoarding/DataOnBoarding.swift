//
//  DataOnBoarding.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 15/07/21.
//

import Foundation
struct Page {
    let image: String
    let title: String
    let text: String
}

let tabs = [
    Page(image: "Picture1", title: "Be Aware In Team", text: "Be aware with your conditionand your teammate screen"),
    Page(image: "Picture2", title: "Recognize Hypotermia", text: "Fast recognize hypotermia symptoms in second"),
    Page(image: "Picture3", title: "Not Use Internet Network", text: "Use bluetooth connecting you and your teammate")
]
