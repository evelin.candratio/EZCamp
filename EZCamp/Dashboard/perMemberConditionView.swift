//
//  perMemberConditionView.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 29/07/21.
//

import SwiftUI

struct perMemberConditionView: View {

    var heartRate = 60
    var bloodOxygen = 90
    var bodyTemp = 30
    var body: some View {
        VStack{
            Text("Personal Summary")
                .font(.system(.title2, design: .rounded))
                .bold()
              //  .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 210))
                .foregroundColor(.white)
                .frame(width: 343, alignment: .leading)
                .padding(.top)
                
        //data Nama teman
                .navigationBarTitle("Friend Name")
            
            HStack {
                
                //Heart Rate
                VStack{
                    HStack{
                        Image("Icon_HeartRateRed")
                            .frame(width: 29.0, height: 40)
                        
                        Text("Heart Rate")
                            .font(.system(.caption, design: .rounded))
                            .bold()
                            .padding(.leading,-8)
                            .foregroundColor(Color(ColorsLibrary.redColor))
                    }
                    .padding(.trailing, 20)
                    
                    Text("\(heartRate) BPM")
                        .font(.system(.title2, design: .rounded))
                        .bold()
                        .padding(.bottom,10)
                        .foregroundColor(Color(ColorsLibrary.redColor))
                    
                    Text("Real Time Updated")
                        .font(.system(.footnote, design: .rounded))
                        .foregroundColor(Color(ColorsLibrary.grayTextColor))
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .frame(width: 60, height:35 , alignment: .center)
                    
                    
                    
                }
                .frame(width: 105, height: 150, alignment: .center)
                .border(Color.black)
                .background(Color(ColorsLibrary.grayItemColor))
                .foregroundColor(.white)
                .cornerRadius(15)
                Spacer()
                
                //Blood Oxygen
                VStack{
                    HStack{
                        Image("Icon_BloodOxyRed")
                            .frame(width: 24.0, height: 40)
                            .imageScale(.large)
                            
                        Text("Blood Oxygen")
                            .font(.system(.caption, design: .rounded))
                            .bold()
                            .foregroundColor(Color(ColorsLibrary.redColor))
                            .padding(.leading,-8)
                    }
                    .padding(.trailing, 20)
                    
                    Text("\(bloodOxygen) %")
                        .font(.system(.title2, design: .rounded))
                        .bold()
                        .padding(.bottom,10)
                        .foregroundColor(Color(ColorsLibrary.redColor))
                    Text("5 Minutes Ago Updated")
                        .font(.system(.footnote, design: .rounded))
                        .foregroundColor(Color(ColorsLibrary.grayTextColor))
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .frame(width: 80, height:35 , alignment: .center)
                    
                    
                    
                }
                .frame(width: 105, height: 150, alignment: .center)
                .border(Color.black)
                .background(Color(ColorsLibrary.grayItemColor))
                .foregroundColor(.white)
                .cornerRadius(15)
                
                Spacer()
                //Body Temperature
                VStack{
                    HStack{
                        Image("Icon_TempRed")
                            .frame(width: 13.0, height: 40)
                            .imageScale(.large)
                        Text("Body Temperature")
                            .font(.system(.caption, design: .rounded))
                            .bold()
                            .padding(.leading,-3.0)
                            .foregroundColor(Color(ColorsLibrary.redColor))
                    }
                    //  .padding(.init(top:10, leading: 0, bottom: 20, trailing: 0))
                    
                    Text("\(bodyTemp) °C")
                        .font(.system(.title2, design: .rounded))
                        .bold()
                        .padding(.bottom,10)
                        .foregroundColor(Color(ColorsLibrary.redColor))
                    Text("Real Time Updated")
                        .font(.system(.footnote, design: .rounded))
                        .foregroundColor(Color(ColorsLibrary.grayTextColor))
                        .lineLimit(2)
                        .frame(width: 60, height:35 , alignment: .center)
                    
                    
                    
                }
                .frame(width: 105, height: 150, alignment: .center)
                .border(Color(red: 0.11, green: 0.11, blue: 0.118))
                .background(Color(ColorsLibrary.grayItemColor))
                .foregroundColor(.white)
                .cornerRadius(15)
                
            }
            .frame(width: 343, alignment: .center)
            .padding(.bottom,5)
            
            // status
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                HStack{
                    Text("Status")
                        .font(.system(.headline, design: .rounded))

                        .fontWeight(.semibold)
                        .padding()
            
                    Spacer()
                    
                    Text("Dangerous Phase")
                        .font(.system(.headline, design: .rounded))
                        .fontWeight(.regular)
                        .multilineTextAlignment(.leading)
                        .foregroundColor(Color(ColorsLibrary.redColor))
                    Image(systemName: "chevron.right")
                        .padding(.trailing)
                }
                
            }).frame(width: 343, height: 50, alignment: .leading)
            .foregroundColor(.white)
            .background(Color(ColorsLibrary.grayItemColor))
            .cornerRadius(15)
            .padding(.bottom)
            
            Text("Disease History")
                .font(.system(.headline, design: .rounded))
                .bold()
                .foregroundColor(.white)
                .frame(width: 343, alignment: .leading)
                .padding(.bottom,5)
            //ambil data di profile
            Text("No history of disease")
                .font(.system(.headline, design: .rounded))
                .foregroundColor(Color(ColorsLibrary.grayTextColor))
                .frame(width: 343, alignment: .leading)
            Spacer()
        }
        
        
    
    }
}

struct perMemberConditionView_Previews: PreviewProvider {
    static var previews: some View {
        perMemberConditionView()
    }
}
