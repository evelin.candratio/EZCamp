//
//  MonitorView.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 16/07/21.
//

import SwiftUI

struct MonitorView: View {
    @State var username = ""
    @State var isOpen = false
    @State var addfriend = false
    @State var isSet = false
    var body: some View {
       // NavigationView {
            ScrollView{
                VStack{
                    if addfriend == false && isSet == false {
                        PersonalSummaryView()
                            .padding(.top)
                        HStack {
                            Text("Friend Condition")
                                .font(.system(.title2, design: .rounded))
                                .bold()
                                .foregroundColor(.white)
                            Spacer()
                            NavigationLink(
                                destination: ConnectBluetoothView(addfriend: $addfriend),
                                label: {
                                    Image(systemName: "person.crop.circle.badge.plus")
                                        .foregroundColor(Color(ColorsLibrary.orangeColor))
                                        .frame(width: 24, height: 24)
                                })
                        }
                        .frame(width: 343, alignment: .leading)
                        .padding(.bottom,10)
                        Text("Please complate your profile to detect your condition")
                            .font(.system(.subheadline, design: .rounded))
                            .multilineTextAlignment(.center)
                            .lineLimit(2)
                            .frame(width: 250, alignment: .center)
                            .foregroundColor(Color(ColorsLibrary.grayTextColor))
                            .padding(.top)
                        
                    } else if addfriend == false && isSet == true{
                        PersonalSummary2()
                            .padding(.top)
                        HStack {
                            Text("Friend Condition")
                                .font(.system(.title2, design: .rounded))
                                .bold()
                                .foregroundColor(.white)
                            Spacer()
                            NavigationLink(
                                destination: ConnectBluetoothView(addfriend: $addfriend),
                                label: {
                                    Image(systemName: "person.crop.circle.badge.plus")
                                        .foregroundColor(Color(ColorsLibrary.orangeColor))
                                        .frame(width: 24, height: 24)
                                })
                            
                           
                        }
                        .frame(width: 343, alignment: .leading)
                        .padding(.bottom,10)
                        Spacer()
                    } else {
                        PersonalSummary2()
                            .padding(.top)
                        HStack {
                            Text("Friend Condition")
                                .font(.system(.title2, design: .rounded))
                                .bold()
                                .foregroundColor(.white)
                            Spacer()
                            
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Image(systemName: "person.crop.circle.badge.plus")
                                    .foregroundColor(Color(ColorsLibrary.orangeColor))
                                    .frame(width: 30, height: 30)
                            })
                        }
                        .frame(width: 343, alignment: .leading)
                        .padding(.bottom,10)
                        MamberSummaryView()
                        Spacer()
                        
                    }
                       
                    }
          
                NavigationLink(destination: CreateProfile(isset: $isSet), isActive: $isOpen){
                }
                
                .navigationBarTitle("\(username)")
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(trailing:
                                        Button(action: {
                                            isOpen = true
                                        }, label: {
                                            Image(systemName: "person.circle.fill")
                                        })
                
//                NavigationLink(
//                    destination: CreateProfile(),
//                    label: {
//                        //Text("create profile")
//                        Image(systemName: "person.circle.fill")
//                            .frame(width: 24, height: 24, alignment: .center)
//                    })
                
                )
              //  .background(Color("BackgroundColor"))
            }.onAppear(){
                var alatSimpan = SaveData()
                username = alatSimpan.loadUsername()
            }
                
            
   //     }
        
        
            
    }
}

struct MonitorView_Previews: PreviewProvider {
    static var previews: some View {
        MonitorView()
    }
}
