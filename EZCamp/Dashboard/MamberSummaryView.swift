//
//  Mamber Summary.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 22/07/21.
//

import SwiftUI

var heart = 60
var oxy = 90
var temp = 30

struct MamberSummaryView: View {
    var body: some View {
        
        VStack {
//            HStack {
//                Text("Friend Condition")
//                    .font(.system(.title2, design: .rounded))
//                    .bold()
//                    .foregroundColor(.white)
//                Spacer()
//                
//                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
//                    Image(systemName: "person.crop.circle.badge.plus")
//                        .foregroundColor(Color(ColorsLibrary.orangeColor))
//                        .frame(width: 24, height: 24)
//                })
//            }
//            .frame(width: 343, alignment: .leading)
//            .padding(.bottom,10)
        
            NavigationLink(
                destination: perMemberConditionView().preferredColorScheme(.dark),
                label: {
                    VStack(alignment: .leading){
                        HStack{
                            //friend name
                            Text("Michael Glee")
                                
                                .bold()
                                .font(.system(size: 18))
                                .foregroundColor(.white)
                            Spacer()
                            Image(systemName: "exclamationmark.triangle")
                            .foregroundColor(Color(red: 0.861, green: 0.008, blue: 0.174))
                            //icon titik 3
                        }
                       .padding(.bottom, -5)

                        HStack(alignment: .top){
                            //heart rate
                            VStack(alignment: .leading) {
                                Image("Icon_HeartRateRed")
                                    .frame(width: 24.0, height: 24.0)
                                    .padding(.bottom,-8)
                                HStack{
                                    Text("\(heart)")
                                        .font(.system(size: 24))
                                    Text("BPM")
                                        .font(.system(size: 12))
                                        .padding(.leading,-5)
                                        .padding(.bottom,-7)
                                }
                                .foregroundColor(Color(red: 0.861, green: 0.008, blue: 0.174))
                                Text("Real Time")
                                    .font(.system(size: 10))
                                    .foregroundColor(Color(ColorsLibrary.grayTextColor))
                            }
                            .frame(width: 60, alignment: .leading)
                            .padding(.trailing, 10.0)
                            
                           
                            
                            //bloodOxy
                            VStack(alignment: .leading) {
                                Image("Icon_BloodOxyRed")
                                    .frame(width: 24.0, height: 24.0)
                                    .padding(.bottom,-8)
                                HStack {
                                    Text("\(oxy)")
                                        .font(.system(size: 24))
                                    Text(" %")
                                        .font(.system(size: 16))
                                        .padding(.leading,-10)
                                        .padding(.bottom,-5)
                                }
                                .foregroundColor(Color(red: 0.861, green: 0.008, blue: 0.174))
                                
                                Text("5 minutes ago update")
                                    .font(.system(size: 10))
                                    .lineLimit(2)
                                    .foregroundColor(Color(ColorsLibrary.grayTextColor))
                            }
                            .frame(width: 60, alignment: .leading)
                            .padding(.trailing, 10.0)
                            
                            
                            //temperatur
                            VStack(alignment: .leading) {
                                Image("Icon_TempRed")
                                    .frame(width: 24.0, height: 24.0)
                                    .padding(.bottom,-8)
                                HStack {
                                    Text("\(temp)")
                                        .font(.system(size: 24))
                                    Text("°C")
                                        .font(.system(size: 16))
                                        .padding(.leading,-7)
            //                            .padding(.bottom,-5)
                                }
                                .foregroundColor(Color(red: 0.861, green: 0.008, blue: 0.174))
                                Text("Real Time")
                                    .font(.system(size: 10))
                                    .foregroundColor(Color(ColorsLibrary.grayTextColor))
                            }
                            .frame(width: 60, alignment: .leading)
                            
                            Spacer()
                            Image(systemName: "chevron.right")
                                .padding(.top,50)
                                .foregroundColor(.white)
                            
                            
                        }
                        .frame(height: 80)

                        
                    }
                    .padding(.horizontal)
                    .frame(width: 343, height: 125, alignment: .leading)
                    .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color(red: 0.11, green: 0.11, blue: 0.118, opacity: 0.851)/*@END_MENU_TOKEN@*/)
                    .cornerRadius(15)
                })
            
            
        }
    }
}

struct MamberSummaryView_Previews: PreviewProvider {
    static var previews: some View {
        MamberSummaryView()
    }
}
