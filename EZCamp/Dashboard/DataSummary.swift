//
//  DataSummary.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 22/07/21.
//

import Foundation
//import UIKit
//
struct member {
    let namaMember: String
    let heartRate: String
    let bloodOxy: String
    let temp : String
    let status : String
}

let condition = [
    member(namaMember: "John Hook", heartRate: "80", bloodOxy: "100", temp: "37", status: "safe"),
    member(namaMember: "Michael Glee", heartRate: "60", bloodOxy: "90", temp: "30", status: "dangerous"),
    member(namaMember: "Alexandra S", heartRate: "64", bloodOxy: "94", temp: "34", status: "middle"),
    member(namaMember: "Michelle N", heartRate: "80", bloodOxy: "99", temp: "37", status: "light")
]
//
//struct Phase {
//    static let safe = UIColor(.white)
//    static let light = UIColor( red: 98/255, green: 221/255, blue: 199/255, alpha: 1.0)
//    static let middle = UIColor( red: 255/255, green: 169/255, blue: 0/255, alpha: 1.0)
//    static let dangerous = UIColor( red: 255/255, green: 118/255, blue: 0/255, alpha: 1.0)
//}


//var arrayUtama = [member]()
//
//
//var arrayListnama = ["John Hook","Michael Glee"]
//
//
//
//var indexpilihan = 1
//
//
//arrayUtama.append(member(namaMember: arrayListnama[indexpilihan], heartRate: "", bloodOxy: "", temp: "", status: ""))
//
