//
//  PersonalSummaryView.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 16/07/21.
//

import SwiftUI

struct PersonalSummaryView: View {
    
    var heartRate = 0
    var bloodOxygen = 0
    var bodyTemp = 0
    
    @StateObject var hr:HealthStore = HealthStore()
    
    var body: some View {
    
        
        VStack(alignment: .center) {
            Text("Personal Condition")
                
                .font(.system(.title2, design: .rounded))
                .bold()
                //  .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 210))
                .foregroundColor(.white)
                .frame(width: 343, alignment: .leading)
            
            
            HStack {
                
                //Heart Rate
                VStack{
                    HStack{
                        Image("Icon_HeartRateWhite")
                            .frame(width: 29.0, height: 40)
                        
                        Text("Heart Rate")
                            .font(.system(.caption, design: .rounded))
                            .bold()
                            .padding(.leading,-8)
                    }
                    .padding(.trailing, 20)
                    
                    Text("\(hr.heartRate) BPM")
                        .font(.system(.title2, design: .rounded))
                        .bold()
                        .padding(.bottom,10)
                        .foregroundColor(Color.white)
                    
                    Text("Real Time Updated")
                        .font(.system(.footnote, design: .rounded))
                        .foregroundColor(Color(ColorsLibrary.grayTextColor))
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .frame(width: 60, height:35 , alignment: .center)
                    
                    
                    
                }
                .frame(width: 105, height: 150, alignment: .center)
                .border(Color.black)
                .background(Color(ColorsLibrary.grayItemColor))
                .foregroundColor(.white)
                .cornerRadius(15)
                Spacer()
                
                //Blood Oxygen
                VStack{
                    HStack{
                        Image("Icon_BloodOxyWhite")
                            .frame(width: 24.0, height: 40)
                            .imageScale(.large)
                        Text("Blood Oxygen")
                            .font(.system(.caption, design: .rounded))
                            .bold()
                            
                            .padding(.leading,-8)
                    }
                    .padding(.trailing, 20)
                    
                    Text("\(bloodOxygen) %")
                        .font(.system(.title2, design: .rounded))
                        .bold()
                        .padding(.bottom,10)
                        .foregroundColor(Color.white)
                    Text("5 Minutes Ago Updated")
                        .font(.system(.footnote, design: .rounded))
                        .foregroundColor(Color(ColorsLibrary.grayTextColor))
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .frame(width: 80, height:35 , alignment: .center)
                    
                    
                    
                }
                .frame(width: 105, height: 150, alignment: .center)
                .border(Color.black)
                .background(Color(ColorsLibrary.grayItemColor))
                .foregroundColor(.white)
                .cornerRadius(15)
                
                Spacer()
                //Body Temperature
                VStack{
                    HStack{
                        Image("Icon_TempWhite")
                            .frame(width: 13.0, height: 40)
                            .imageScale(.large)
                        Text("Body Temperature")
                            .font(.system(.caption, design: .rounded))
                            .bold()
                            .padding(.leading,-3.0)
                    }
                    //  .padding(.init(top:10, leading: 0, bottom: 20, trailing: 0))
                    
                    Text("\(bodyTemp) °C")
                        .font(.system(.title2, design: .rounded))
                        .bold()
                        .padding(.bottom,10)
                        .foregroundColor(Color.white)
                    Text("Real Time Updated")
                        .font(.system(.footnote, design: .rounded))
                        .foregroundColor(Color(ColorsLibrary.grayTextColor))
                        .lineLimit(2)
                        .frame(width: 60, height:35 , alignment: .center)
                    
                    
                    
                }
                .frame(width: 105, height: 150, alignment: .center)
                .border(Color(red: 0.11, green: 0.11, blue: 0.118))
                .background(Color(ColorsLibrary.grayItemColor))
                .foregroundColor(.white)
                .cornerRadius(15)
                
            }
            .frame(width: 343, alignment: .center)
            .padding(.bottom)
            
            // status
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                HStack{
                    Text("Status")
                        .font(.system(.headline, design: .rounded))
                        
                        .fontWeight(.semibold)
                        .padding()
                    
                    Spacer()
                    
                    Text("Safe and Sound ")
                        .font(.system(.headline, design: .rounded))
                        .fontWeight(.regular)
                        .multilineTextAlignment(.leading)
                    
                    Image(systemName: "chevron.right")
                        .padding(.trailing)
                }
                
            }).frame(width: 343, height: 50, alignment: .leading)
            .foregroundColor(.white)
            .background(Color(ColorsLibrary.grayItemColor))
            .cornerRadius(15)
            
            //member summary
            
            
            .onAppear {
                
                //hr = HealthStore()
                //print (hr)
                if  hr != nil{
                    hr.lastHeartRate(completion: {result in
                        hr.heartRate = result
                    })
                }
            }
            
            HStack {
                Spacer()
                HStack{
                    Image("Icon_ FaseNormal")
                        //                        .padding(.leading, -10.0)
                        .frame(width: 13.0, height: 13.0)
                    Text("Safe and Sound")
                        .font(.system(.caption2, design: .rounded))
                        .lineLimit(2)
                        .foregroundColor(.white)
                    // .padding(.leading, -1)
                    
                }
                .frame(width: 80, height: 30)
                Spacer()
                
                HStack{
                    Image("Icon_DangerBlue")
                        .padding(.leading, -20.0)
                    Text("Light Phase")
                        .font(.system(.caption2, design: .rounded))
                        .lineLimit(2)
                        .foregroundColor(Color(red: 0.007, green: 0.478, blue: 0.998))
                    //   .padding(.leading, -10.0)
                    
                }
                .frame(width: 60, height: 30)
                Spacer()
                
                HStack{
                    Image("Icon_DangerYellow")
                        .padding(.leading, -12.0)
                    Text("Middle Phase")
                        .font(.system(.caption2, design: .rounded))
                        .foregroundColor(Color(red: 0.999, green: 0.661, blue: 0.014))
                        .lineLimit(2)
                    // .padding(.leading, -10.0)
                    
                }
                .frame(width: 80, height: 30)
                Spacer()
                
                HStack{
                    Image("Icon_DangerRed")
                        .padding(.leading, -10.0)
                    Text("Dangerous Phase")
                        .font(.system(.caption2, design: .rounded))
                        .foregroundColor(Color(red: 0.861, green: 0.008, blue: 0.174))
                        .lineLimit(2)
                        // .padding(.leading, -10.0)
                        .frame(width: 60, height: 30)
                }
                .frame(width: 70, height: 30)
                Spacer()
            }
            .frame(width: 343, height: 50, alignment: .center)
            .foregroundColor(.white)
        }
        
    }
    
}

struct PersonalSummaryView_Previews: PreviewProvider {
    static var previews: some View {
        PersonalSummaryView()
    }
}
