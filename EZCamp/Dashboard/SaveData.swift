//
//  SaveData.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 03/08/21.
//

import Foundation
class SaveData {
    func saveUsername(username:String){
        UserDefaults.standard.set(username, forKey: "EZCamp_username")
    }
    func loadUsername()->String{
        return UserDefaults.standard.string(forKey: "EZCamp_username") ?? "Ini Joko"
    }
}
