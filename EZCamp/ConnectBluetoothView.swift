//
//  ConnectBluetoothView.swift
//  EZCamp
//
//  Created by Steven Tan on 28/07/21.
//

import SwiftUI

struct ConnectBluetoothView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @Binding var addfriend : Bool
    
    @State var arrayUtama = [member]()
    var arrayListnama = ["John Hook","Michael Glee","Alexandra S","Michelle N"]
    @State var textconnect = "Not Connected"
    var indexPilihan = 1
//    arrayUtama.append(member(namaMember: arrayListnama[indexpilihan], heartRate: "", bloodOxy: "", temp: "", status: ""))
    
    
    
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text("Close").bold().foregroundColor(.orange)
                })
                
                Text("Add Member").bold().foregroundColor(.white).frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                Button(action: {
                    presentationMode.wrappedValue.dismiss();
                    addfriend = true
                }, label: {
                    Text("Done").bold().foregroundColor(.orange)
                })
            }.frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading).padding(.init(top: 27, leading: 9, bottom: 10, trailing: 9)).background(Color.black)
            
            List{
                HStack{
                    Text("Michael Glee")
                    Spacer()
                    Text(textconnect).frame(alignment: .trailing)
                        .onTapGesture {
                            // To Do: Call Connect BLE Device
                            if textconnect == "Not Connected" {
                                self.textconnect = "Connected"
                            } else if textconnect == "Connected" {
                                self.textconnect = "Not Connected"
                            }
                        }
                }
                HStack{
                    Text("Alexandra S")
                    Spacer()
                    Text("Not Connected").frame(alignment: .trailing)
                }

                HStack{
                    Text("Michelle N")
                    Spacer()
                    Text("Not Connected").frame(alignment: .trailing)

                }
               
            }
            
//            List{
//                    ForEach(0...arrayListnama.count-1,id: \.self) { model in
//
//                        HStack {
//                            Text("\(arrayListnama[model])")
//                            Spacer()
//                            Text(text).frame(alignment: .trailing)
//                                .onTapGesture {
//                                    // To Do: Call Connect BLE Device
//                                    if text == "Not Connect" {
//                                        self.text = "Connected"
//                                    } else if text == "Connected" {
//                                        self.text = "Not Connect"
//                                    }
//                                }
//                        }
//                }
//            }
               
                    }
        .navigationBarTitle("Add Friend")
            //.onAppear(perform: connectBLEDevice)
//
    }
}


//private func connectBLEDevice(){
//    let ble = BLEConnection()
//    // Start Scanning for BLE Devices
//    ble.startCentralManager()
//}

//struct DaftarList: View {
//    @State var text = "Not Connect"
//    var body: some View {
//        HStack{
//            Text("")
//            Text(text).frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .trailing)
//                .onTapGesture {
//                    // To Do: Call Connect BLE Device
//                    if text == "Not Connect" {
//                        self.text = "Connected"
//                    } else if text == "Connected" {
//                        self.text = "Not Connect"
//                    }
//                }
//        }
//    }
//}

// UIHosting Controller
//var child = UIHostingController(rootView: ContentView())

//struct ConnectBluetoothView_Previews: PreviewProvider {
//    static var previews: some View {
//        ConnectBluetoothView()
//    }
//}
