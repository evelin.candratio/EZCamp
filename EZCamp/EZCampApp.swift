//
//  EZCampApp.swift
//  EZCamp
//
//  Created by Evelin Candratio on 13/07/21.
//

import SwiftUI

@main
struct EZCampApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
