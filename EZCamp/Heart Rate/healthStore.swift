//
//  healthStore.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 25/07/21.
//

import Foundation
import HealthKit
import SwiftUI

class HealthStore:ObservableObject {
    var healthStore : HKHealthStore?
    @Published var heartRate:String = ""
    
    init() {
        if HKHealthStore.isHealthDataAvailable() {
            healthStore = HKHealthStore ()
        }
    }
    
    func requestAuthorization(complation: @escaping (Bool)-> Void) {
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        
        guard let healthStore = self.healthStore else { return complation(false) }
        
        healthStore.requestAuthorization(toShare: [heartRateType], read: [heartRateType]) { (success, error)  in
            if (success){ print("success authorize")
            }
           // complation(success)
        }
        
    }
    func lastHeartRate(completion:@escaping (String)->()){
        
        guard let sampleType = HKObjectType.quantityType(forIdentifier: .heartRate) else {
            return
        }
        let startDate = Calendar.current.date(byAdding: .month, value: -1, to: Date())
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: .strictEndDate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        let query = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors: [sortDescriptor]) {
            (sample, result, error ) in
            print("TEst")
          //  print (result)
            guard error == nil else {
                return
            }
            print(result?.count)
            
            if result!.count > 0 {
                let data = result![0] as! HKQuantitySample
                let unit = HKUnit(from: "count/min")
                let latestHr = data.quantity.doubleValue(for: unit)
                print("lastest Hr\(latestHr) BPM")
                
                //self.heartRate = "\(latestHr)"
                completion("\(latestHr)")
                
                let dateFormator = DateFormatter()
                dateFormator.dateFormat = "dd/MM/yyy hh:mm s"
                let startDate = dateFormator.string(from: data.startDate)
                let endtDate = dateFormator.string(from: data.endDate)
                print("startDate \(startDate) : endDate \(endtDate)")
            }else{
                completion("0")
            }
            
            
            
            
            
        }
        healthStore?.execute(query)
    }
    
}
