//
//  PhotoProfile.swift
//  EZCamp
//
//  Created by Evelin Candratio on 23/07/21.
//

import SwiftUI

struct PhotoProfile: View {
    @State var imageData : Data = .init(capacity: 0)
    @State var show = false
    @State var imagepicker = false
    @State var source : UIImagePickerController.SourceType = .photoLibrary
    
    var body: some View {
        ZStack{
            NavigationLink(destination: ImagePicker(show: $imagepicker, image: $imageData, source: source), isActive: $imagepicker){
                Text("")
            }
            
            VStack{
                ZStack{
                    Circle()
                        .fill(Color(red: 44/255, green: 44/255, blue: 46/255))
                        .frame(width: 150, height: 150)
                    if imageData.count != 0 {
                        Image(uiImage:UIImage(data: self.imageData)!)
                            .resizable()
                            .frame(width: 150, height: 150)
                            .clipShape(Circle())
                            .foregroundColor(Color(red: 44/255, green: 44/255, blue: 46/255))
                    } else {
                        Image(systemName: "camera.fill")
                            .font(.system(size: 40))
                            .foregroundColor(Color(red: 1, green: 0.463, blue: 0))
                            .frame(width: 150, height: 150)
                            .clipShape(Circle())
                    }
                }
                
                Button(action: {
                    self.show.toggle()
                }){
                    Text("Take a Photo")
                        .foregroundColor(Color(red: 1, green: 0.463, blue: 0))
                }
            }.actionSheet(isPresented: $show) {
                ActionSheet(title: Text("Take a photo or select from photo library"), message: Text(""), buttons:
                    [.default(Text("Photo Library"), action: {
                        self.source = .photoLibrary
                        self.imagepicker.toggle()
                    }),.default(Text("Camera"), action:{
                        self.source = .camera
                        self.imagepicker.toggle()
                        
                    })]
                )
            }
        }
    }
}

struct PhotoProfile_Previews: PreviewProvider {
    static var previews: some View {
        PhotoProfile()
    }
}
