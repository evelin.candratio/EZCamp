//
//  CreateProfile.swift
//  EZCamp
//
//  Created by Evelin Candratio on 15/07/21.
//

import SwiftUI


//struct CreateProfile: View {
//    @State private var image: Image?
//
//    @State private var showingImagePicker = false
//    @State private var inputImage: UIImage?
//
//    var body: some View {
//        ZStack{
//            VStack{
//                HStack{
//                    ZStack{
//                        Circle()
//                            .fill(Color.secondary)
//                            .frame(width: 150, height: 150)
//
//                        if image != nil{
//                            image?
//                                .resizable()
//                                .frame(width: 150, height: 150)
//                                .cornerRadius(75)
//                        } else {
//                            Image(systemName: "camera.fill")
//                                .font(.system(size: 40))
//                                .foregroundColor(Color(red: 1, green: 0.463, blue: 0))
//                                .cornerRadius(75)
//                        }
//
//
//                    }
//                    .onTapGesture{
//                        self.showingImagePicker = true
//                    }
//                }
//            }
//            .sheet(isPresented: $showingImagePicker, onDismiss: loadImage) {
//                ImagePicker(image: self.$inputImage)
//            }
//        }
//    }
//
//    func loadImage() {
//        guard let inputImage = inputImage else { return }
//        image = Image(uiImage: inputImage)
//    }
//}

struct CreateProfile: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var isset : Bool
    @State var username: String = ""
    @State var password: String = ""
    @State var dateOfBirth = Date()
    @State var selectedGenderIndex: Int = 0
    @State var diseaseHistory: String = ""
    var alatSimpan = SaveData()
    var genderOptions = ["Male", "Female", "Other"]
    
    var body: some View {
            VStack{
                PhotoProfile()
                    .padding(.bottom)
                Form{
                    Section(header: Text("Username")){
                        TextField("Your Username", text: $username)
                    }
                    Section(){
                        DatePicker("Date of Birth", selection: $dateOfBirth, in: ...Date(), displayedComponents: .date)
                        Picker("Gender", selection: $selectedGenderIndex) {
                            ForEach(0..<genderOptions.count) {
                                Text(self.genderOptions[$0])
                            }
                        }
                    }
                    Section(header: Text("Disease History")){
                        TextEditor(text: $diseaseHistory)                    }
                    
                }
                
               
                
                .navigationBarItems(trailing:  Button(action: {
                    alatSimpan.saveUsername(username: username); presentationMode.wrappedValue.dismiss(); isset = true
                }, label: {
                    Text("Done")
                }))
                
    }
    
}
}


//struct CreateProfile_Previews: PreviewProvider {
//    static var previews: some View {
//        CreateProfile()
//    }
//}

