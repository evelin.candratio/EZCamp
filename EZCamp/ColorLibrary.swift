//
//  ColorLibrary.swift
//  EZCamp
//
//  Created by Septia Rosalina Malik on 28/07/21.
//

import Foundation
import UIKit

struct ColorsLibrary {
    static let orangeColor = UIColor( red: 255/255, green: 118/255, blue: 0/255, alpha: 1.0)
    static let blueColor = UIColor( red: 98/255, green: 221/255, blue: 199/255, alpha: 1.0)
    static let redColor = UIColor( red: 255/255, green: 69/255, blue: 58/255, alpha: 1.0)
    static let grayItemColor = UIColor( red: 28/255, green: 28/255, blue: 30/255, alpha: 1.0)
    static let grayTextColor = UIColor( red: 142/255, green: 142/255, blue: 147/255, alpha: 1.0)
}
